use postgres::types::Type;
use postgres::{Client, NoTls, Statement};

use super::config;
use super::sys_status::SystemStatus;

pub struct Store {
    db: Client,
    stmt: Statement,
    id: String,
}

impl Store {
    pub fn new(id: String, config: &config::Config) -> Result<Self, postgres::Error> {
        let mut db = postgres::Config::new()
            .password(config.dbpass.clone())
            .user(config.dbuser.as_str())
            .host(config.dbhost.as_str())
            .dbname(config.dbname.as_str())
            .port(config.dbport)
            .connect(NoTls)?;
        let stmt = db.prepare_typed(
            "INSERT INTO status (hostname, temperature, average_cpu_load, memory_usage)
                VALUES ($1, $2, $3, $4);",
            &[Type::VARCHAR, Type::FLOAT4, Type::FLOAT4, Type::FLOAT4],
        )?;

        Ok(Store { db, stmt, id })
    }

    pub fn insert_status(&mut self, status: &SystemStatus) -> Result<(), postgres::Error> {
        // Should probably be a better error handling here
        match self.db.query(
            &self.stmt,
            &[&self.id, &status.temp, &status.load_average, &status.memory],
        ) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}
