use systemstat::{Platform, System};

#[derive(Debug)]
pub struct SystemStatus {
    pub load_average: f32,
    pub temp: f32,
    pub memory: f32,
}

impl SystemStatus {
    pub fn new() -> Self {
        Self {
            load_average: 0.0,
            temp: 0.0,
            memory: 0.0,
        }
    }

    pub fn update(&mut self, sys: &System) {
        self.load_average = match sys.load_average() {
            Ok(ldavg) => ldavg.one,
            Err(err) => {
                eprintln!("{}", err);
                self.load_average
            }
        };
        self.temp = match sys.cpu_temp() {
            Ok(temp) => temp,
            Err(err) => {
                eprintln!("{}", err);
                self.temp
            }
        };

        self.memory = match sys.memory() {
            Ok(mem) => mem.free.as_u64() as f32 / mem.total.as_u64() as f32,
            Err(err) => {
                eprintln!("{}", err);
                self.memory
            }
        }
    }
}
