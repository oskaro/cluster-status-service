use std::fs;
use std::time::Duration;
use yaml_rust::{Yaml, YamlLoader};

/// Used to hold relevant data for the service
pub struct Config {
    pub dbname: String,
    pub dbpass: String,
    pub dbuser: String,
    pub dbhost: String,
    pub dbport: u16, 
    pub poll_interval: Duration,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            poll_interval: Duration::from_secs(10),
            dbname: Default::default(),
            dbpass: Default::default(),
            dbuser: Default::default(),
            dbhost: Default::default(),
            dbport: Default::default(),
        }
    }
}

impl Config {
    /// Extract config from a yaml object.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use yaml_rust::YamlLoader;
    /// let y = YamlLoader::load_from_str("
    /// DBNAME: postgres
    /// DBHOST: localhost
    /// DBUSER: postgres
    /// PDBASS: example
    /// POLL_INTERVAL: 10
    /// ").unwrap();
    ///
    ///let config = Config::from_yaml(y);
    /// assert_eq!(config.dbname, "postgres")
    /// assert_eq!(config.dbhost, "localhost")
    /// assert_eq!(config.dbuser, "postgres")
    /// assert_eq!(config.dbpass, "example")
    /// ```
    pub fn from_yaml(conf: &Yaml) -> Self {
        let poll_interval = match conf["POLL_INTERVAL"].as_f64() {
            Some(interval) => Duration::from_secs_f64(interval),
            None => Duration::from_secs(10),
        };

        let dbport = match conf["DBPORT"].as_i64() {
            Some(port) => port as u16,
            None => 5432,
        };

        Config {
            dbname: conf["DBNAME"].as_str().unwrap_or("").into(),
            dbpass: conf["DBPASS"].as_str().unwrap_or("").into(),
            dbhost: conf["DBHOST"].as_str().unwrap_or("").into(),
            dbuser: conf["DBUSER"].as_str().unwrap_or("").into(),
            dbport,
            poll_interval,
        }
    }
    pub fn from_file(path: &str) -> Result<Self, &str> {
        let yaml = load_yaml_config(path)?;
        Ok(Self::from_yaml(&yaml))
    }
}

pub fn load_yaml_config(path: &str) -> Result<Yaml, &str> {
    let conf = match fs::read_to_string(path) {
        Ok(fh) => fh,
        Err(_) => return Err("Could not open file"),
    };
    let conf = match YamlLoader::load_from_str(conf.as_str()) {
        Ok(c) => c,
        Err(_) => return Err("Could not load yaml"),
    };
    Ok(conf[0].clone())
}
