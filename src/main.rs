extern crate gethostname;
extern crate postgres;
extern crate systemstat;
extern crate yaml_rust;

mod config;
mod store;
mod sys_status;

use config::Config;
use store::Store;
use systemstat::{Platform, System};

use std::thread::sleep;

const CONF_LOCATION: &str = "/etc/cluster-status/conf.yml";

fn main() {
    let conf = Config::from_file(CONF_LOCATION).expect("Could not load config!");

    let id = String::from(gethostname::gethostname().to_str().unwrap());
    println!("Starting with ID: {}", id);

    let mut store = Store::new(id, &conf).expect("Could not create store!");

    let mut data = sys_status::SystemStatus::new();
    let sys = System::new();

    loop {
        sleep(conf.poll_interval);
        data.update(&sys);
        store.insert_status(&data).expect("Could not insert status");
    }
}
