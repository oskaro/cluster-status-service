# Cluster Status Service

A simple service that logs the status of a cluster node to a database.

## Config

A config.yml file is required at the location from which you run the project.
It needs to have all database info to set up.
